package id.bootcamp.note.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import org.jetbrains.annotations.Nullable

@Parcelize //bawahnya tambahin implement 'parcelable'
@Entity(tableName = "note")
class NoteEntity(

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    val id: Int,

    @field:ColumnInfo(name = "title")
    val title: String,

    @field:ColumnInfo(name = "description")
    val description: String,

    @field:ColumnInfo(name = "created_date")
    val createdDate: String,

    @field:ColumnInfo(name = "modified_date")
    val modifiedDate: String?
) : Parcelable