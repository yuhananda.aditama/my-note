package id.bootcamp.note.data.local.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import id.bootcamp.note.data.local.entity.ExampleEntity
import id.bootcamp.note.data.local.entity.NoteEntity

@Dao
interface NoteDao {
    @Query("SELECT * FROM NOTE ORDER BY ID ASC")
    suspend fun getAllNotes() : List<NoteEntity>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(noteEntity: NoteEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateNote(noteEntity: NoteEntity)

    @Query("DELETE FROM NOTE WHERE ID = :id")
    suspend fun deleteNoteById(id : Int)

    @Delete
    suspend fun deleteNote(noteEntity: NoteEntity)
}