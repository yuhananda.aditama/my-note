package id.bootcamp.note.data

import id.bootcamp.note.data.local.ExampleSharedPreference
import id.bootcamp.note.data.local.entity.NoteEntity
import id.bootcamp.note.data.local.room.ExampleDao
import id.bootcamp.note.data.local.room.NoteDao
import id.bootcamp.note.data.remote.retrofit.RegresApiService
import id.bootcamp.note.ui.data.ExampleData
import id.bootcamp.note.ui.data.ExamplePage

//1. Ubah konstruktor sesuai datasourc yang kita pakai (dao saja)
//2. sesuaikan fungsi get instance yang dibawah
class NoteRepository private constructor(
    private val noteDao: NoteDao
) {

    //Buat suspend fungsi untuk mengambil data note dari database
    suspend fun getAllNotes() : List<NoteEntity>{
        val dataList : List<NoteEntity> = noteDao.getAllNotes()
        return dataList
    }

    //Buat suspend fungsi untuk insert
    suspend fun insertNote(noteEntity: NoteEntity){
        noteDao.insertNote(noteEntity)
    }

    //Buat suspend fungsi untuk update
    suspend fun updateNote(noteEntity: NoteEntity){
        noteDao.updateNote(noteEntity)
    }

    //Buat suspend fungsi untuk delete
    suspend fun deleteNote(id : Int){
        noteDao.deleteNoteById(id)
    }

    companion object {
        @Volatile
        private var instance: NoteRepository? = null
        fun getInstance(
            noteDao: NoteDao
        ): NoteRepository =
            instance ?: synchronized(this) {
                instance ?: NoteRepository(noteDao)
            }.also { instance = it }
    }
}