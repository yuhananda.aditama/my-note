package id.bootcamp.note.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.note.data.NoteRepository
import id.bootcamp.note.data.local.entity.NoteEntity
import kotlinx.coroutines.launch

class NoteViewModel(val noteRepository: NoteRepository) : ViewModel() {
    //Buat object / variabel tipe datanya LiveData(List<NoteEntity>)
    val noteListLiveData = MutableLiveData<List<NoteEntity>>()

    //Buat fungsi untuk mengambil data list NoteEntity di repository
    fun getAllNotes() = viewModelScope.launch {
        //Ubah value dari object /variabel live data di atasnya
        val dataList = noteRepository.getAllNotes()
        noteListLiveData.postValue(dataList)
    }

}