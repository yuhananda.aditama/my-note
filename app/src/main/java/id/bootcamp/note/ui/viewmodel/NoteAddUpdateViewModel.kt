package id.bootcamp.note.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.note.data.NoteRepository
import id.bootcamp.note.data.local.entity.NoteEntity
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.launch

class NoteAddUpdateViewModel(val noteRepository: NoteRepository) : ViewModel() {

    //1. Buat fungsi untuk insert ke repo
    fun insert(noteEntity: NoteEntity) = viewModelScope.launch {
        noteRepository.insertNote(noteEntity)
    }

    //2. Buat fungsi untuk update ke repo
    fun update(noteEntity: NoteEntity) = viewModelScope.launch {
        noteRepository.updateNote(noteEntity)
    }

    //3. Buat fungsi untuk delete ke repo
    fun delete(id: Int) = viewModelScope.launch {
        noteRepository.deleteNote(id)
    }


}