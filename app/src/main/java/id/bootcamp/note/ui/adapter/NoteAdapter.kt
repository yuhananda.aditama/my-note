package id.bootcamp.note.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import id.bootcamp.note.R
import id.bootcamp.note.data.local.entity.NoteEntity
import id.bootcamp.note.ui.data.ExampleData

class NoteAdapter(val listNote: ArrayList<NoteEntity>) :
    RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    private var onNoteItemListener: OnNoteItemListener? = null

    fun setOnNoteItemListener(onNoteItemListener: OnNoteItemListener) {
        this.onNoteItemListener = onNoteItemListener
    }

    interface OnNoteItemListener {
        fun onItemClick(noteEntity: NoteEntity)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDate = itemView.findViewById<TextView>(R.id.tv_item_date)
        val tvTitle = itemView.findViewById<TextView>(R.id.tv_item_title)
        val tvDesc = itemView.findViewById<TextView>(R.id.tv_item_description)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_note, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoteAdapter.ViewHolder, position: Int) {
        val data = listNote[position]
        holder.tvDate.text = data.createdDate
        holder.tvTitle.text = data.title
        holder.tvDesc.text = data.description

        if (onNoteItemListener != null) {
            holder.itemView.setOnClickListener {
                onNoteItemListener?.onItemClick(data)
            }
        }
    }

    override fun getItemCount(): Int {
        return listNote.size
    }
}