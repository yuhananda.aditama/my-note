package id.bootcamp.note.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.note.data.ExampleRepository
import id.bootcamp.note.data.NoteRepository
import id.bootcamp.note.di.Injection

//1. Ubah konstruktor jadi noteRepository
//2. sesuaikan bagian companion object fungsi getInstance
class NoteViewModelFactory private constructor(private val noteRepository: NoteRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NoteViewModel::class.java)) {
            return NoteViewModel(noteRepository) as T
        } else if (modelClass.isAssignableFrom(NoteAddUpdateViewModel::class.java)) {
            return NoteAddUpdateViewModel(noteRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    companion object {
        @Volatile
        private var instance: NoteViewModelFactory? = null
        fun getInstance(context: Context): NoteViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: NoteViewModelFactory(Injection.provideNoteRepository(context))
            }.also { instance = it }
    }
}