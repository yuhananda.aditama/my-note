package id.bootcamp.note.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.bootcamp.note.R
import id.bootcamp.note.data.local.entity.NoteEntity
import id.bootcamp.note.databinding.ActivityNoteBinding
import id.bootcamp.note.ui.adapter.NoteAdapter
import id.bootcamp.note.ui.viewmodel.NoteViewModel
import id.bootcamp.note.ui.viewmodel.NoteViewModelFactory

class NoteActivity : AppCompatActivity() {
    //Deklarasikan / Buat lateinit Variabel
    //1. binding
    private lateinit var binding: ActivityNoteBinding

    //2. viewmodel
    private lateinit var viewModel: NoteViewModel

    //3. adapter
    private lateinit var mAdapter: NoteAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //inisialisasikan
        //1. binding
        binding = ActivityNoteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //2.viewmodel
        viewModel = ViewModelProvider(
            this,
            NoteViewModelFactory.getInstance(this)
        ).get(NoteViewModel::class.java)

        //Observer buat live data
        viewModel.noteListLiveData.observe(this) {
            if (it != null) {
                binding.rvNotes.layoutManager = LinearLayoutManager(this)
                mAdapter = NoteAdapter(ArrayList(it)) //List -> ArrayList
                binding.rvNotes.adapter = mAdapter
                //implementasi interface
                mAdapter.setOnNoteItemListener(object : NoteAdapter.OnNoteItemListener {
                    override fun onItemClick(noteEntity: NoteEntity) {
                        //Tambahkan aksi untuk pindah ke halaman NoteAddUpdateActivity
                        //Bawakan juga data noteEntity ke halaman tersebut dengan key "data"
                        val intent = Intent(this@NoteActivity,
                            NoteAddUpdateActivity::class.java)
                        intent.putExtra("data",noteEntity)
                        startActivity(intent)
                    }
                })
            }
        }
        //panggil fungsi getAllNotes di viewmodel
        viewModel.getAllNotes()

        //Buat aksi ketika click floating action button
        binding.fabAdd.setOnClickListener {
            //pindah ke halaman NoteAddUpdateActivity
            val intent = Intent(this, NoteAddUpdateActivity::class.java)
            startActivity(intent)
        }

    }
}