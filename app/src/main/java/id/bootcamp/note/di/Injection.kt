package id.bootcamp.note.di

import android.content.Context
import id.bootcamp.note.data.ExampleRepository
import id.bootcamp.note.data.NoteRepository
import id.bootcamp.note.data.local.ExampleSharedPreference
import id.bootcamp.note.data.local.room.ExampleDatabase
import id.bootcamp.note.data.local.room.NoteDao
import id.bootcamp.note.data.local.room.NoteDatabase
import id.bootcamp.note.data.remote.retrofit.ApiConfig

object Injection {
    fun provideExampleRepository(context: Context):ExampleRepository{
        val exampleApiService = ApiConfig.getRegresApiService()
        val exampleDatabase = ExampleDatabase.getInstance(context)
        val exampleDao = exampleDatabase.exampleDao()
        val exampleSharedPreference = ExampleSharedPreference(context)
        return ExampleRepository.getInstance(exampleApiService,exampleDao,exampleSharedPreference)
    }

    //buat fungsi baru provideNoteRepository
    fun provideNoteRepository(context: Context) : NoteRepository{
        //didalam fungsi, buatlah noteDatabase & noteDao
        //create database, jika belum ada
        val noteDatabase : NoteDatabase = NoteDatabase.getInstance(context)
        val noteDao : NoteDao = noteDatabase.noteDao()
        //Return value NoteRepository.getInstance
        return NoteRepository.getInstance(noteDao)
    }
}